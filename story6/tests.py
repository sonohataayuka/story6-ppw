from django.test import TestCase
from django.apps import apps
from django.test import Client
from django.urls import resolve, reverse
from .apps import Story6Config
from .models import Activity, Guest
from .forms import ActivityForm, GuestForm
from .views import addActivity, listActivity, deleteActivity, deleteUser, join

# Create your tests here.
class ModelTest(TestCase):
    def setUp(self):
        self.activity = Activity.objects.create(activity_name="belajar")
        self.guest = Guest.objects.create(guest_name="Ayuka")

    def test_instance_created(self):
        self.assertEqual(Activity.objects.count(), 1)
        self.assertEqual(Guest.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.activity), "belajar")
        self.assertEqual(str(self.guest), "Ayuka")

class FormTest(TestCase):
    def test_form_is_valid(self):
        activity_form = ActivityForm(data={
            "activity_name": "belajar",
        })
        self.assertTrue(activity_form.is_valid())
        guest_form = GuestForm(data={
            'guest_name': "Ayuka"
        })
        self.assertTrue(guest_form.is_valid())

    def test_form_invalid(self):
        activity_form = ActivityForm(data={})
        self.assertFalse(activity_form.is_valid())
        guest_form = GuestForm(data={})
        self.assertFalse(guest_form.is_valid())

class UrlsTest(TestCase):

    def setUp(self):
        self.activity = Activity.objects.create(activity_name = "belajar")
        self.guest = Guest.objects.create(
            guest_name = "Alifah", activity = Activity.objects.get(activity_name="belajar"))
        self.listActivity = reverse("story6:listActivity")
        self.addActivity = reverse("story6:addActivity")
        self.join = reverse("story6:join", args=[self.activity.pk])
        self.delete = reverse("story6:delete", args=[self.guest.pk])
        self.deleteActivity = reverse("story6:deleteActivity", args=[self.activity.pk])


    def test_listActivity_use_right_function(self):
        found = resolve(self.listActivity)
        self.assertEqual(found.func, listActivity)

    def test_addActivity_use_right_function(self):
        found = resolve(self.addActivity)
        self.assertEqual(found.func, addActivity)

    def test_join_use_right_function(self):
        found = resolve(self.join)
        self.assertEqual(found.func, join)

    def test_delete_use_right_function(self):
        found = resolve(self.delete)
        self.assertEqual(found.func, deleteUser)

    def test_deleteActivity_use_right_function(self):
        found = resolve(self.deleteActivity)
        self.assertEqual(found.func, deleteActivity)


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.listActivity = reverse("story6:listActivity")
        self.addActivity = reverse("story6:addActivity")

    def test_GET_listActivity(self):
        response = self.client.get(self.listActivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'list2.html')

    def test_GET_addActivity(self):
        response = self.client.get(self.addActivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'add2.html')

    def test_POST_addActivity(self):
        response = self.client.post(self.addActivity,
        {
            'activity_name': 'belajar',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'list2.html')

    def test_POST_addActivity_invalid(self):
        response = self.client.post(self.addActivity,
        {
            'activity_name': '',
        }, follow=True)
        self.assertTemplateUsed(response, 'add2.html')

    def test_GET_delete(self):
        activity = Activity(activity_name="belajar")
        activity.save()
        guest = Guest(guest_name="Ayuka",
                      activity = Activity.objects.get(pk=1))
        guest.save()
        response = self.client.get(reverse('story6:delete', args=[guest.pk]))
        self.assertEqual(Guest.objects.count(), 0)
        self.assertEqual(response.status_code, 302)

    def test_GET_deleteActivity(self):
        activity = Activity(activity_name="belajar")
        activity.save()
        guest = Guest(guest_name="Ayuka",
                      activity = Activity.objects.get(pk=1))
        guest.save()
        response = self.client.get(reverse('story6:deleteActivity', args=[guest.pk]))
        self.assertEqual(Guest.objects.count(), 0)
        self.assertEqual(response.status_code, 302)

class TestRegist(TestCase):
    def setUp(self):
        activity = Activity(activity_name="abc")
        activity.save()

    def test_regist_POST(self):
        response = Client().post('/join/1/',
                                 data={'guest_name': 'ayuka'})
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = self.client.get('/join/1/')
        self.assertTemplateUsed(response, 'join.html')
        self.assertEqual(response.status_code, 200)

    def test_regist_POST_invalid(self):
        response = Client().post('/join/1/',
                                 data={'nama': ''})
        self.assertTemplateUsed(response, 'join.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story6Config.name, 'story6')
        self.assertEqual(apps.get_app_config('story6').name, 'story6')

