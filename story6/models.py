from django.db import models

 
# Create your models here.
class Activity(models.Model):
    activity_name = models.CharField(max_length = 100)
   
    def __str__(self):
        return self.activity_name
 
class Guest(models.Model):
    guest_name = models.CharField(max_length=64)
    activity = models.ForeignKey(Activity,
    on_delete=models.CASCADE, null=True, blank=True)
    def __str__(self):
        return self.guest_name
