
from django.urls import path, include
from django.contrib import admin
from . import views
# from . import views
 
app_name = 'story6'
 
urlpatterns = [
    path('addActivity/', views.addActivity, name='addActivity'),
    path('listActivity/', views.listActivity, name='listActivity'),
    path('delete/<int:delete_id>/', views.deleteUser, name='delete'),
    path('deleteActivity/<int:delete_id>/', views.deleteActivity, name='deleteActivity'),
    path('join/<int:task_id>/', views.join, name='join'),
    # path('url-yang-diinginkan/', views.nama_fungsi, name='nama_fungsi')
    # path('profile/', views.profile, name='profile'),
    # dilanjutkan ...
]
