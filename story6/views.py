from . import forms
from .models import Activity, Guest
from django.shortcuts import render, redirect
from .forms import ActivityForm, GuestForm

# Create your views here.

def addActivity(request):
    activity_form = forms.ActivityForm()
    if request.method == 'POST':
        activity_form_input = forms.ActivityForm(request.POST)
        if activity_form_input.is_valid():
            data = activity_form_input.cleaned_data
            activity_input = Activity()
            activity_input.activity_name = data['activity_name']
            activity_input.save()
            # current_data = Activity.objects.all()

            return redirect('/listActivity')  
        else:
            # current_data = Activity.objects.all()
            return render(request, 'add2.html',{'form':activity_form, 'status':'failed'})
    else:
        # current_data = Activity.objects.all()
        return render(request, 'add2.html',{'form':activity_form})


def listActivity(request):
    activity = Activity.objects.all()
    guest = Guest.objects.all()
    return render(request, 'list2.html',{'activity': activity, 'guest':guest})

def deleteUser(request, delete_id):
    # if request.method == "POST":
    activity = Activity.objects.all()
    guest = Guest.objects.all()
    print(delete_id)
    guest_to_delete = Guest.objects.get(id=delete_id)
    guest_to_delete.delete()
    # return render(request, 'story6/listActivity.html', {'kegiatan': kegiatan, 'orang': orang})
    return redirect('/listActivity')

def deleteActivity(request, delete_id):
    try:
        deleted_activity = Activity.objects.get(pk = delete_id)
        deleted_activity.delete()
        return redirect('/listActivity')
    except:
        return redirect('/listActivity')

def join(request, task_id):
    guest_form = forms.GuestForm()
    if request.method == 'POST':
        guest_form_input = forms.GuestForm(request.POST)
        if guest_form_input.is_valid():
            data = guest_form_input.cleaned_data
            newGuest = Guest()
            newGuest.guest_name = data['guest_name']
            newGuest.activity = Activity.objects.get(id = task_id)
            newGuest.save()
            return redirect('/listActivity')  
        else:
            return render(request, 'join.html',{'form':guest_form, 'status':'failed'})
    else:
        return render(request, 'join.html',{'form':guest_form})
