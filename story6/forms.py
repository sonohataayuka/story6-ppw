from django import forms 
 
class ActivityForm(forms.Form):
    activity_name = forms.CharField(
        label = "Activity name",
        widget = forms.TextInput(
            attrs = {
                'class':'form-control',
                'max_length : 100'
                'id' : 'fname',
                'placeholder' : 'Activity name',
            }
        )
    )
 
 
class GuestForm(forms.Form):
    guest_name = forms.CharField(
        label = "Your name",
        widget = forms.TextInput(
            attrs = {
                'class':'form-control',
                'max_length : 100'
                'id' : 'fname',
                'placeholder' : 'Your name',
            }
        )
    )
