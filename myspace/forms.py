from django import forms 

class SubjectForm(forms.Form):
    subject_name = forms.CharField(
        label = "Subject name",
        widget = forms.TextInput(
            attrs = {
                'class':'form-control',
                'max_length : 100'
                'id' : 'fname',
                'placeholder' : 'Subject name',
            }
        )
    )

    credits_amount = forms.IntegerField(
        label = "Amount of credits",
        widget = forms.NumberInput(
            attrs={
                'class':'form-control',
                'min':'1',
                'max':'6',
                'id' : 'fname',
                'placeholder' : 'Amount of credits',
            }
        )
    )

    room = forms.CharField(
        label = "Room", 
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'max_length : 100'
                'id' : 'fname',
                'placeholder' : 'Room',
            }
        )
    )

    lecturer = forms.CharField(
        label = "Lecturer name", 
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'max_length : 100'
                'id' : 'fname',
                'placeholder' : 'Lecturer name',
            }
        )
    )

    description = forms.CharField(
        label = "Subject description", 
        widget = forms.Textarea(
            attrs={
                'class':'form-control',
                'max_length : 100'
                'cols' : '30',
                'rows' : '10',
                'id' : 'fname',
                'placeholder' : 'Subject description',
            }
        )
    )
    
    year = forms.CharField(
        label = "Semester", 
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'max_length : 100'
                'id' : 'fname',
                'placeholder' : 'Semester',
            }
        )
    )
