from django.db import models

# Create your models here.
class Subject(models.Model):
    subject_name = models.CharField(max_length = 100)
    credits_amount = models.IntegerField()
    room = models.CharField(max_length = 100)
    lecturer = models.CharField(max_length = 100)
    year = models.CharField(max_length = 100)
    description = models.CharField(max_length = 1000, null = True)

    def __str__(self):
        return self.subject_name
